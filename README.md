# 8051 Dev Board 

Development board for 8051 mcu (28DIP)

## Gettin started

This project was developed with the open source KiCAD software. The design is for a 10 x 10 cm dual-layer pcb board.

### Prerequisites

* [KiCAD](http://www.kicad-pcb.org/) - A Cross Platform and Open Source Electronics Design Automation Suite


